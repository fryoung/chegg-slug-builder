import {createSlug, isValidSlug} from '../src/utils'

describe('utils test suite', () => {
    it('createSlug should create a valid slug', () => {

        expect(createSlug({
            subject: 'Biology',
            classification: 'General Biology',
            topic: 'Photosynthesis'
        })).toEqual('/subject/biology/classification/general-biology/topic/photosynthesis');

        // check for space and special character replacement
        expect(createSlug({
            subject: 'Dogs',
            classification: 'Dog Events & Celebrations',
            topic: 'Dog Weddings, OMG!'
        })).toEqual('/subject/dogs/classification/dog-events-and-celebrations/topic/dog-weddings-omg');

        expect(createSlug({
            subject: 'San Francisco',
            classification: 'Nightlife',
            topic: 'Bars & Clubs'
        })).toEqual('/subject/san-francisco/classification/nightlife/topic/bars-and-clubs');
    });

    it('createSlug should return undefined if form values are not passed', () => {
        // @ts-ignore
        expect(createSlug(undefined)).toBeUndefined()
    });

    it('createSlug should return partial slug', () => {
        expect(createSlug({
            subject: 'Biology'
        })).toEqual('/subject/biology');

        expect(createSlug({
            subject: 'Biology',
            classification: 'General Biology'
        })).toEqual('/subject/biology/classification/general-biology');        
    });

    it('isValidSlug should return false when passed an invalid or incomplete slug', ()=> {
        expect(isValidSlug('')).toEqual(false)
        expect(isValidSlug('/subject/biology')).toEqual(false)
    });

    it('isValidSlug should return true when passed a complete slug', ()=> {
        expect(isValidSlug('/subject/biology/classification/general-biology/topic/photosynthesis')).toEqual(true)
    });
})