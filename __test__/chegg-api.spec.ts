import {getSubjects, getClassifications, getTopics, getFieldNames, getQuery} from '../src/chegg-api'
import {SelectorField, OptionItem} from '../src/types'
import {Fields} from '../src/constants'
 
describe('chegg-api test suite',() => {
    test('getSubjects should return list of subjects', async () =>{
       const subject:SelectorField = await getSubjects({}) as SelectorField;
       expect(subject).not.toBeNull();
       expect(subject.options).not.toBeNull();
       const options = subject.options as OptionItem[];
       expect(options[0].id).toEqual('biology');
       expect(options[1].id).toEqual('business');
    });

    test('getClassifications should return list of classifications', async () =>{
        const classification:SelectorField = await getClassifications({subject: 'civil engineering'}) as SelectorField;
        expect(classification).not.toBeNull();
        expect(classification.options).not.toBeNull();
        const options = classification.options as OptionItem[]; 
        expect(options[0].id).toEqual('structural design');
        expect(options[1].id).toEqual('analysis of structures');
     });

     test('getClassifications should return empty array when bad subject is passed', async () =>{
      const classification:SelectorField = await getClassifications({subject: 'NOPE'}) as SelectorField;
      const options = classification.options as OptionItem[]
      expect(options).toHaveLength(0)
   });     

   test('getTopics should return list of topics', async () =>{
        const topic:SelectorField = await getTopics({subject: 'math', classification:'trigonometry' }) as SelectorField;
        expect(topic).not.toBeNull();
        expect(topic.options).not.toBeNull();
        const options = topic.options as OptionItem[];
        expect(options[0].id).toEqual('0-trigonometry');
        expect(options[1].id).toEqual('1-trigonometry');
   });

   test('getTopics should return null options when passed invalid query', async () => {
      await expect(getTopics({subject: 'NOPE', classification:'DOUBLE-NOPE' }))
        .rejects
        .toEqual('could not find match with subject=NOPE, classification=DOUBLE-NOPE');
   });

   test('getFieldNames should return list of required fields used in api calls', ()=> {
        expect(getFieldNames()).toEqual([Fields.SUBJECT, Fields.CLASSIFICATION, Fields.TOPIC]);
   });

   test('getQuery should return a promise given a field name and valid Query', ()=> {
      expect(getQuery(Fields.SUBJECT, {})).toBeInstanceOf(Promise);      
      expect(getQuery(Fields.CLASSIFICATION, {subject: 'biology'})).toBeInstanceOf(Promise);
      expect(getQuery(Fields.TOPIC, {subject: 'biology', classification: 'general biology'})).toBeInstanceOf(Promise);
   });

   test('getQuery should return null if given an invalid fieldName', ()=> {
      expect(getQuery('NOPE', {})).toBeNull()
   });


})