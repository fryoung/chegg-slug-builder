## chegg-contentful-slug-builder

#### What?
POC concept of a slug-builder for Chegg content entries that forces the slugs to be contructed from (TDB) API responses to ensure accurate and unique slugs

#### Requirements
This POC uses the `create-contentful-extension` to simplify the scaffolding and deploy processes.
https://github.com/contentful/create-contentful-extension

You should read about steps need to enable debugging while inside the contentful web app.

#### Discussion

While the `create-contentful-extension` makes building and deploying your UI Extension a breeze, there were a few things that I came across that were annoying.

1. As of today (2019-12-12) Many dependencies in the create-contentful-app are a bit out of date. React, Typescript, jest, etc are all a bit dated, but still work. I had to modify my eslintrc and to support some language features I needed.

2. Debugging in Chrome Tools is problematic. Not sure if it's an issue with this create-contentful-extension or if I was missing something in my .tsconfig or something else, but all my breakpoints were not source mapped correctly so the browser would break in the wrong place almost all the time. When it did work, it seemed random.

3. Some of the examples I found already blew past the 512kb limitation for srcdoc (contentful hosted) UI extensions, so I had to make sure that I wasn't importing things that I wasn't using. Luckily when you build it the cli will tell you how big your bundle is and allow you to see all the packed sizes of dependencies and modules

4. Contentful documentation is decent, but expect to do a lot of googling to accomplish any real-world examples.

5. The UI Extension SDK is pretty robust, but it doesn't support all of the CMA (Content Management API) features yet, so just be aware of that. 

#### Installation
```
npm install
```

#### Configuration
Configure your ui-extension by choosing your space and environment during the configure step
```
npm run configure
```

The `extension.json` file is where you can set custom vars that can be consumed by your extension for various purposes.

example: https://github.com/contentful/extensions/blob/master/samples/slug/extension.json


#### Development
Running `npm start` will install your ui-extension in development mode in the space/environment you selected in the configure step. 
```
npm start

--

// you should see the following output in your console:

Installing extension in development mode...

✨  Successfully created extension:

Space: [space-id]
Environment: [environment-id]
Your extension: https://app.contentful.com/spaces/[space-id]/environments/[environment-id]/settings/extensions/chegg-slug-builder

```


#### Deploying to Contentful

```
npm run deploy
```

will build your ui-extension, compress it, and then inline it into a single html blob and then deploy it to the contentful environment you specified earlier when you ran `npm run configure`. 

When you view your extension's config page in contentful settings:
`[x] Hosted by Contentful (srcdoc) checkbox will be checked`
`A minified inline version of it will be in the code box`



#### Note: 
After building your extension, you can install it in other environments and spaces using the `contentful-cli extension` command 

https://github.com/contentful/contentful-cli/tree/master/docs/extension


**From the root directory**:
```
contentful extension create --environment-id=master --descriptor=./extension.json
```

##### Notes
If there were errors when building your ui-extension or if your ui-extension is larger than the maximum size (512kb), the deployment will fail


#### Testing

**Unit Tests**
```
npm run test
```

**Coverage**
```
npm run test:coverage
```


#### References
CLI
https://www.contentful.com/developers/docs/tools/cli/

SDK
https://contentful.github.io/contentful.js/contentful/7.10.0/

Migrations
https://github.com/contentful/contentful-migration/blob/master/README.md

UI Extensions Examples on GitHub
https://github.com/contentful/extensions/tree/master/samples

Looks Interesting for Testing!
https://www.npmjs.com/package/contentful-mock
