import {NameValuePair, GenericObject} from './types'

/**
 * Helper method to create a slug from Subject, Classification, and Topic combinations
 * @param formValues 
 */
export function createSlug<T>(formValues: GenericObject):string | null | undefined {
  if(!formValues) {
    console.error('createSlug requires a valid formValues hash')
    return
  }

  const subject:NameValuePair = {name: 'subject', value: formValues.subject}
  const classification:NameValuePair = {name: 'classification', value: formValues.classification}
  const topic:NameValuePair = {name: 'topic', value: formValues.topic}

  const slug: string = [subject, classification, topic].reduce((rtn, field)=>{
    if(field.value) {
      rtn = `${rtn}/${field.name}/${field.value}`
    }
    return rtn;
  }, '')

  return slug.toLowerCase()
    .replace(/\s/g, "-")
    .replace(/&/g, 'and')
    .replace(/[,!]/g,'')
}

/**
 * Returns true / false if slug is valid or not
 * @param  {string}
 * @return {boolean}
 */
export function isValidSlug(slug: string):boolean {
  if(typeof slug === "undefined" || slug.length === 0) {
    return false;
  }
  const valid = !!slug.match(/^(\/)*subject\/[\w-]*\/classification\/[\w-]*\/topic\/[\w-]*$/)
  return valid
}

/**
 * @param {string} slug
 */
// export function slugToParams(slug: string):object {
//   const matches = slug.match(/^\/subject\/([\w-]*)\/classification\/([\w-]*)\/topic\/([\w-]*)$/);
//   if(!matches || matches.length < 4) {
//        console.warn('malformed slug')
//       return {}
//   }
//   const subject:string = matches[1].replace(/-/g, ' ')
//   const classification:string = matches[2].replace(/-/g, ' ')
//   const topic:string = matches[3].replace(/-/g, ' ').replace('and', '&')
//   return {
//       subject,
//       classification,
//       topic,
//       slug
//   }
// }

