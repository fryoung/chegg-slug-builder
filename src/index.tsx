import * as React from 'react';
import { render } from 'react-dom';
import { init, locations, EditorExtensionSDK, EntryFieldAPI } from 'contentful-ui-extensions-sdk';

// importing these components individially because treeshaking is not working...
import DisplayText from '@contentful/forma-36-react-components/dist/components/Typography/DisplayText';
import SectionHeading from '@contentful/forma-36-react-components/dist/components/Typography/SectionHeading';
import Paragraph from '@contentful/forma-36-react-components/dist/components/Typography/Paragraph';
import TextInput from '@contentful/forma-36-react-components/dist/components/TextInput';
import TextField from '@contentful/forma-36-react-components/dist/components/TextField';
import Form from '@contentful/forma-36-react-components/dist/components/Form/Form';

// custom components
import ErrorBoundary from './components/ErrorBoundary/ErrorBoundary'
import TopicSelector from './components/TopicSelector'
import AutoComplete from './components/AutoComplete'
import Taxonomy from './components/Taxonomy'

import '@contentful/forma-36-react-components/dist/styles.css';
import '@contentful/forma-36-fcss/dist/styles.css';

import { getSubjects, getClassifications, getTopics, getFieldNames, getQuery} from './chegg-api'
import { isValidSlug, createSlug } from './utils'
import { Fields } from './constants'
import {OptionItem, SelectorField, CheggData, Query, GenericObject} from './types'
import './index.css';
import Spinner from '@contentful/forma-36-react-components/dist/components/Spinner';
import { instanceOf } from 'prop-types';

/**
 * To use this demo create a Content Type with the following fields:
 *  title: Short text
 *  slug: Short Text
 *  subject: Short Text
 *  classification: Short Text
 *  topic: Short Text
 */

interface AppProps {
  sdk: EditorExtensionSDK;
}

interface AppState {
  data: CheggData;
  values: {[key: string]: string | undefined | null};  
  isLoading: boolean;
  editorVisible: boolean;
}

export class App extends React.Component<AppProps, AppState> {

  _isMounted:boolean = false;
  detachExternalChangeHandler:Function | undefined;

  constructor(props: AppProps) {
    super(props);

    const {sdk} = props
    const fields = sdk ? sdk.entry.fields : {}
    const values:GenericObject = {}

    // iterate through sdk entry fields and assign to values object in state
    Object.keys(fields).forEach( fieldName => {
      values[fieldName] = fields[fieldName].getValue()
    });

    this.state = {
      isLoading: true,
      editorVisible: !isValidSlug(values.slug as string),
      data: {
        subject: null,
        classification: null,
        topic: null,
      },
      values
    };
  }

  componentDidMount() {
    this._isMounted = true
    const { sdk } = this.props
    if(sdk) {
      // do sdk stuff
      // Handler for external field value changes (e.g. when multiple authors are working on the same entry).
      // This also gets called when you make a change to a field value using the sdk vs direct user input
      // @ts-ignore -- note: this method is incorrectly typed in the contentful type definitions
      this.detachExternalChangeHandler = sdk.entry.fields.slug.onValueChanged(this.onExternalChange);      
    }

    this.getInitialData()
  }
  
  componentWillUnmount(){
    this._isMounted = false
  }

  /**
   * populates component after initially mounted
   */
  getInitialData(): void {
    const values:GenericObject = this.state.values
    const fields = getFieldNames()
    const calls:Promise<SelectorField>[] = [];
    const queries: {[key: string]: string | null} = {};

    // if the UI already has values for subject, classification, topics, then let's get that data so we can pre-populate the dropdowns
    fields.forEach( (field:string) => {
        if(values[field]) {
          let query:Query = {[field]: values[field] as string}
          Object.keys(queries).forEach(fieldName => {
            query[fieldName] = queries[fieldName]
          })
          queries[field] = query[field];
          calls.push(getQuery(field, query) as Promise<SelectorField>);
        }
    });

    if(calls.length) {
      Promise.all(calls).then((response: any) => {
        const mappedResponse = Object.keys(response).map( (key: string) => response[key]) as SelectorField[];
        //console.info('>>> onDataLoaded ARRAY', mappedResponse, ' isArray = ', Array.isArray(mappedResponse), Object.values(values));
        this.onDataLoaded(mappedResponse);
      });
    }
    else {
        this.loadData(Fields.SUBJECT, {})
    }
  }

  /**
   * After API data is loaded, this method constructs and updates the component state
   * @param data 
   */
  onDataLoaded (data: CheggData | SelectorField[]): void {
    /* istanbul ignore next */ 
    if(!this._isMounted){
      // don't update state if component is not mounted
      return;
    }

    if(Array.isArray(data)) {
        const merged: CheggData = data.reduce((r, val) => {
            r[`${val.id}`] = val
            return r
        }, {} as unknown as CheggData)
        this.setState({
          isLoading: false,
          data: merged
        })
    }
    else {
      this.setState({
        isLoading: false,        
        data
      });
    }
  }

  /**
   * This method is called when user selects new a Subject/Classification/Topic from the dropdowns in the TopicSelector component
   * @param  {String}
   * @return {[type]}
   */
  loadData = async (fieldName: string, query: Query = {}) => {
      const { data } = this.state;
      const changes:{[key: string]: string | null} = {};
      let response;      

      this.setState({ isLoading: true});

      switch(fieldName) {
          case Fields.SUBJECT:
              response = await getSubjects({});
              changes[Fields.CLASSIFICATION] = null
              changes[Fields.TOPIC] = null
              break;
          case Fields.CLASSIFICATION:
              response = await getClassifications(query);
              changes[Fields.TOPIC] = null
              break;
          case Fields.TOPIC:
              response = await getTopics(query);
              break;
      }

      const dataParams:CheggData = {
        ...data,
        ...changes, 
        [fieldName]: {...response}
      } as unknown as CheggData
      this.onDataLoaded(dataParams);
  }

  onExternalChange = (e: Event) => {
    console.info('slug changed externally', e);
  }

  /**
   * handler for edit slug link click
   */
  onEditSlugClickHandler = () => {
      this.setState({
          editorVisible: true
      })
  }

  /**
   * handler for when topic is changed (only used by the Autosuggest component)
   */
  onTopicChange = (value: string) => {
      this.abstractOnFieldChangeHandler({
        id: 'topic',
        value
      });
  }

  /**
   * handler for when component title is updated
   */
  onTitleChangeHandler = async (event: React.ChangeEvent<HTMLInputElement>) => {
    const { values } = this.state
    const value = event.target.value;
    this.setState({ values: {
      ...values,
      title: value
    } });
    await this.props.sdk.entry.fields.title.setValue(value);
  }

  /**
   * handler for select box onchange events
   */
  onFieldChangeHandler = async (event: React.ChangeEvent<HTMLInputElement>) => {

    const field:HTMLInputElement = event.target;
    const fieldId:string = field.getAttribute('id') as string
    const fieldValue:string | number = field.value

    this.abstractOnFieldChangeHandler({
      id: fieldId,
      value: fieldValue
    })
  };

  onSlugBuilderClose = () => {
     this.setState({
      editorVisible: false
     })
  }

  /**
   * handles state updates for when component values are changed
   */
  abstractOnFieldChangeHandler = async ({id: fieldId, value: fieldValue}: {id: string, value: string}) => {
    const { sdk } = this.props
    const changes:Query = {}
    const values:GenericObject = {
      ...this.state.values,
      [fieldId]: fieldValue
    }

    switch(fieldId) {
      case Fields.SUBJECT:
          changes[Fields.TOPIC] = null;
          changes[Fields.CLASSIFICATION] = null;
          this.loadData(Fields.CLASSIFICATION, values as Query);          
          break;
      case Fields.CLASSIFICATION:
          changes[Fields.TOPIC] = null;  
          this.loadData(Fields.TOPIC, values as Query)
          break;
    }

    const adjustedValues:GenericObject = {    
      ...values,
      ...changes,      
    }

    const slug:string = createSlug(adjustedValues) as string;
    const validSlug:boolean = isValidSlug(slug as string)

    this.setState({
       values: {
         ...adjustedValues,
         slug
       },
       editorVisible: !validSlug
    });
 
    if(sdk) {
      const fields:{[fieldName: string]: EntryFieldAPI} = sdk ? sdk.entry.fields : {}      
      if(fields[fieldId]) {
        await fields[fieldId].setValue(fieldValue);
      }
      if(validSlug) {
        await fields.slug.setValue(slug);
        sdk.notifier.success('slug saved!')
      }
    }     
  }

  /**
   * Returns array of SelectorField objects to pass to the TopicSelector component
   * Fields are only returned if they exist in the state (from previous API call)
   * @return {SelectorField[]}
   */
  getFields = () => {
    const { data, values } = this.state
    const fields: SelectorField[] = []
    const fieldNames: string[] = getFieldNames()

    fieldNames.forEach(fieldName => {
      const field:SelectorField = data[fieldName] as SelectorField  
      if(field && Object.keys(field).length) {
          if(values[field.id]){
              field.value = values[field.id];
          }
          if(field.name) {
             fields.push(field);            
          }
      }
    });
    return fields
  }

  render() {

    const { isLoading, values, data, editorVisible } = this.state
    const fields = this.getFields()
    const topics: SelectorField = data.topic as SelectorField
    const taxonomyProps = {
      subject: values.subject,
      classification: values.classification,
      topic: values.topic
    }

    return (
      <Form className="f36-margin--l">
        <DisplayText>Chegg Slug-Builder Demo</DisplayText>
        <Paragraph>
          This demo uses an Entry UI Extension to render the whole editor for an entry.
        </Paragraph>
        <SectionHeading>Title</SectionHeading>
        <TextInput
          data-testid="field-title"
          onChange={this.onTitleChangeHandler}
          value={values.title || ''}
        />
        {fields.length === 0 && <Spinner />}
        <Taxonomy fields={taxonomyProps} />
        {editorVisible && 
          <React.Fragment>
            <TopicSelector
              isLoading={isLoading}
              fields={fields}
              onFieldChangeHandler={this.onFieldChangeHandler}
              onClose={this.onSlugBuilderClose}
            />
          </React.Fragment>
       }
       {/* AutoComplete component still needs work; currently not rendered in page  */}
       {false && <AutoComplete 
            items={topics.options as OptionItem[]} 
            onChange={value => {
              this.abstractOnFieldChangeHandler({
                id: 'topics',
                value
              })
            }}
        />}
        <TextField
            id="slug"
            name="slug"
            data-testid="field-slug"
            labelText="Slug"
            textLinkProps={{
              icon: 'Edit',
              text: 'edit slug',
              className: 'edit-slug-button',
              onClick: this.onEditSlugClickHandler,
            }}
            value={values.slug as string}
            textInputProps={{
              disabled: true,
            }}
        />
      </Form>
    );
  }
}

/* istanbul ignore next */ 
init(sdk => {
  if (sdk && sdk.location.is(locations.LOCATION_ENTRY_EDITOR)) {
    render(
       <ErrorBoundary>
            <App sdk={sdk as EditorExtensionSDK} />
        </ErrorBoundary>, 
        document.getElementById('root')
      );
  }
});

/**
 * By default, iframe of the extension is fully reloaded on every save of a source file.
 * If you want to use HMR (hot module reload) instead of full reload, uncomment the following lines
 */
 // if (module.hot) {
 //   module.hot.accept();
 // }
