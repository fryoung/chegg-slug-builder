import React, { FormEvent } from 'react'
import Autosuggest, {
    // types
    RenderSuggestionParams,
    InputProps, 
    ChangeEvent
} from 'react-autosuggest'

import {OptionItem} from '../../types'

const escapeRegexCharacters = (str: string) => str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
const getSuggestionValue = (suggestion:OptionItem) => suggestion.name;
const renderSuggestion = (suggestion: OptionItem, query?: RenderSuggestionParams) => {
    if(query) {
        // do something different
    }
    return <span>{suggestion.name}</span>
};

export interface AutoCompleteProps {
    items: OptionItem[]
    onChange(value: string): void;
}

export interface AutoCompleteState {
    suggestions: OptionItem[]
    // user entered string
    value: string
    ready: boolean
}

/**
 * Wrapper for react-autosuggest
 */
export default class AutoComplete extends React.Component<AutoCompleteProps, AutoCompleteState> {

    constructor(props: AutoCompleteProps) {
       super(props)
       this.state = {
              suggestions: [],
              value: '',
              ready: false
       }
    }

    componentDidMount() {
        if(this.props.items) {
            this.onReady()
        } 
    }

    onReady() {
        this.setState({
            ready: true
        })
    }

    onSuggestionsFetchRequested = ({ value }: {value: string}): void => {
        const suggestions = this.getSuggestions(value)
        console.log('onSuggestionsFetchRequested', suggestions)
        this.setState({suggestions});
    }

    onSuggestionsClearRequested = (): void => {
        this.setState({ suggestions:[]});
    }

    getSuggestions = (value: string) => {
      const { items } = this.props
      const escapedValue = escapeRegexCharacters(value.trim());
      if (escapedValue === '') { return [];}
      const regex = new RegExp(escapedValue, 'i');
      return items.filter(item => regex.test(item.name));
    };    

     onChange = (event: React.FormEvent, params: Autosuggest.ChangeEvent) => {
        this.setState({
          value: params.newValue
        });
        this.props.onChange(params.newValue);
     };

    render<TSuggestion>() {
       const { value, suggestions, ready } = this.state;
       const inputProps: InputProps<OptionItem> = {
          placeholder: "search for topics",
          value,
          onChange: this.onChange
        };

        if(!ready){
            return null
        }
        // console.info('AutoSuggestRender: ', JSON.stringify(suggestions))

        return (
          <Autosuggest // eslint-disable-line react/jsx-no-undef
            data-testid="auto-suggest"
            suggestions={suggestions}
            getSuggestionValue={getSuggestionValue}
            renderSuggestion={renderSuggestion}
            inputProps={inputProps}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
            onSuggestionsClearRequested={this.onSuggestionsClearRequested}            
          />
        );    
    }
}