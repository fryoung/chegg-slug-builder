import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import AutoComplete  from './AutoComplete'
import {OptionItem} from '../../types'

function renderComponent({items, onChange}: {items: OptionItem[], onChange: (v:string)=>void}) {
    return render(
        <AutoComplete 
           items={items} 
           onChange={onChange}
    />);
}

describe('AutoComplete Test Suite', () => {

    afterEach(cleanup);

    test('it should render', () => {
         const onChange = jest.fn()
         const { getByRole } = renderComponent({
             onChange,
             items: [{id:'nutrition', value: 'nutrition', name:'Nutrition'}]
         });

         expect(getByRole('combobox')).not.toBeNull();
    })

    test('it should handle empty items', () => {
        const onChange = jest.fn()
        const { queryByRole } = render(<AutoComplete 
            // @ts-ignore
            items={undefined} 
            onChange={onChange}
        />);

        const result = queryByRole('combobox');
        expect(result).toBeNull();
   })    


    test('it should render list when user enters text', async () => {
        const onChange = jest.fn()
        const { getByRole, queryAllByRole } = renderComponent({
            onChange,
            items: [
                {id:'nutrition', value: 'nutrition', name:'Nutrition'},
                {id:'zoology', value: 'zoology', name:'Zoology'},
                {id:'general biology', value: 'general biology', name:'General Biology'}
            ]
        });

        const combobox:HTMLDivElement = getByRole('combobox') as HTMLDivElement;
        const searchBox:HTMLInputElement = combobox.querySelector('input') as HTMLInputElement

        searchBox.focus();
        fireEvent.change(searchBox, { target: { value: 'ogy' }});
        fireEvent.change(searchBox, { target: { value: '' }});
        fireEvent.change(searchBox, { target: { value: '?*' }});

        const listbox = queryAllByRole('listbox');
        expect(listbox).not.toBeNull();


        // setTimeout(async ()=> {
        //    console.log('OPTIONZZ', container.querySelectorAll('li'));           
        //    done()
        // }, 200)

        // @ts-ignore
        //const flushPromises = () => new Promise(setImmediate);
        // await wait(() => findAllByRole('option'));
        // const options:Array<HTMLLIElement> = getAllByRole('option') as Array<HTMLLIElement>;
    });
    

})