/* istanbul ignore file */
import * as React from 'react';
import DisplayText from '@contentful/forma-36-react-components/dist/components/Typography/DisplayText';
import Paragraph from '@contentful/forma-36-react-components/dist/components/Typography/Paragraph';

interface ErrorBoundaryProps {
  children: JSX.Element
}

interface ErrorBoundaryState {
  error?: Error
  hasError: boolean
}

export default class ErrorBoundary extends React.Component<ErrorBoundaryProps,ErrorBoundaryState> {
  constructor(props: ErrorBoundaryProps) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error: Error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true, error };
  }

  componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
    // You can also log the error to an error reporting service
      console.error('ErrorBoundary caught error ', error, errorInfo)
  }

  render() {
    const { error, hasError } = this.state
    if (hasError) {
      // You can render any custom fallback UI
      return (
        <div className='error-page'>
          <DisplayText>Something went wrong.</DisplayText>
          <Paragraph>
            {JSON.stringify(error)}
          </Paragraph>
        </div>
      )
    }
    return this.props.children; 
  }
}