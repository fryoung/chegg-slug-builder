import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import TopicSelector, {SelectorProps} from './TopicSelector'
import { getSubjects } from '../../chegg-api'

function renderComponent({fields, isLoading, onFieldChangeHandler, onClose}: SelectorProps) {
    return render(
        <TopicSelector 
            fields={fields} 
            isLoading={isLoading} 
            onFieldChangeHandler={onFieldChangeHandler}
            onClose={onClose ? onClose : ()=> {}}
       />
    );
}

describe('TopicSelector Test Suite', () => {

    afterEach(cleanup);    

    test('Should not render anything when fields are empty', () => {
        const onFieldChangeHandler = jest.fn()        
        const { queryByTestId } = renderComponent({
            fields: null, 
            onFieldChangeHandler,
            isLoading: true,
            onClose: ()=>{ }
        });
        expect(queryByTestId('chegg-topic-selector')).toBeNull()
    });

    test('Should render subjects dropdown', async () => {

        const onFieldChangeHandler = jest.fn()
        const onCloseHandler = jest.fn()
        const fields = [await getSubjects({})];  
        const { getAllByTestId } = renderComponent({
            fields, 
            onFieldChangeHandler,
            isLoading: false,
            onClose: onCloseHandler
        });

        expect(getAllByTestId('cf-ui-select--subject').length).toBeGreaterThan(0);
        const options = getAllByTestId('cf-ui-select-option') as HTMLInputElement[];
        const subjectIds:string[] = ['biology', 'business', 'chemistry', 'civil engineering', 'computer science']
        let matchCount = 0

        options.forEach((opt: HTMLInputElement) => {
             if(subjectIds.includes(opt.value)){
                 matchCount++;
             }
         });
         expect(matchCount).toEqual(subjectIds.length)
    });
    
    test('Should call onFieldChangeHandler when Subject dropdown is called', async () => {
        const onFieldChangeHandler = jest.fn()
        const fields = [await getSubjects({})];
        const onCloseHandler = jest.fn()
        const { findByLabelText } = renderComponent({
            fields, 
            onFieldChangeHandler,
            isLoading: false,
            onClose: onCloseHandler
        });
        
        const dd:HTMLSelectElement = await findByLabelText('Subject') as HTMLSelectElement
        fireEvent.change(dd, {target: {
            value: 'chemistry'
        }});
        expect(dd.value).toEqual('chemistry');
        expect(onFieldChangeHandler).toHaveBeenCalled();
    });


    test('Should render <Spinner> when isLoading is true', async () => {
        const onFieldChangeHandler = jest.fn()
        const fields = [await getSubjects({})];
        const onCloseHandler = jest.fn()
        const { getByTestId } = renderComponent({
            fields, 
            onFieldChangeHandler,
            isLoading: true,
            onClose: onCloseHandler
        });
        expect(getByTestId('spinner')).not.toBeNull();
    });


})