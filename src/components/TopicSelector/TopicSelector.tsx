import * as React from 'react';
import Spinner from '@contentful/forma-36-react-components/dist/components/Spinner';
import FieldGroup from '@contentful/forma-36-react-components/dist/components/Form/FieldGroup';
import SelectField from '@contentful/forma-36-react-components/dist/components/SelectField/SelectField';
import Option from '@contentful/forma-36-react-components/dist/components/Select/Option';
import SectionHeading from '@contentful/forma-36-react-components/dist/components/Typography/SectionHeading';
import IconButton from '@contentful/forma-36-react-components/dist/components/IconButton';
import {SelectorField} from '../../types'

export interface SelectorProps {
    fields: SelectorField[] | null;
    isLoading: boolean;
    onFieldChangeHandler: (event: React.ChangeEvent<HTMLInputElement>) => void
    onClose: (event?: React.MouseEvent) => void
}

export default function TopicSelector({ fields, isLoading, onFieldChangeHandler, onClose }: SelectorProps): React.ReactElement | null {
    if(!fields || fields.length === 0) {
      return null;
    }

    return (
        <FieldGroup 
            data-testid="chegg-topic-selector" 
            className="chegg-topic-selector" 
            row={false}
        >
        <IconButton 
           label='' 
           iconProps={{icon:'Close'}}
           buttonType='secondary'
           onClick={onClose}
           className='close-button'
        />
        <SectionHeading>Slug Builder</SectionHeading>            
        {fields.map( field => (
          <SelectField
            key={field.id}
            data-testid={`cf-ui-select--${field.id}`}
            id={field.id}
            name={field.name}
            labelText={field.name}
            value={field.value || ''}
            onChange={onFieldChangeHandler}
          >
            <Option value="">Pick a {field.name}</Option>
            {field.options && field.options.length && field.options.map((item) => {
              return <Option data-testid="cf-ui-select-option" key={item.id} value={item.id}>{item.name}</Option>
            })}
          </SelectField>
      ))}
      { isLoading && <Spinner data-testid="spinner" /> }          
    </FieldGroup>)
}