import * as React from 'react';

function ReadOnlyText ({value, testid = '--read-only'}: {value: string, testid?: string}): JSX.Element{
    return <div className='chegg-read-only-input' data-testid={testid}>{value}</div>	
}

export default ReadOnlyText;