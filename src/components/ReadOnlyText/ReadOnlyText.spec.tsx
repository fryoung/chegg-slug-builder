import React from 'react';
import { render } from '@testing-library/react';
import ReadOnlyText  from './ReadOnlyText'

function renderComponent({value, testid}: {value: string, testid: string | undefined}) {
    return render(
        <ReadOnlyText 
            value={value} 
            testid={testid}
       />);
}

test('ReadOnlyComponent should render with specified text', () => {
	const slug = '/subject/chemistry/classification/organic-chemistry/topic/0-organic-chemistry'
    const { getByTestId } = renderComponent({ value: slug, testid: 'read-only-text'});
    const result = getByTestId('read-only-text');
    expect(result.innerHTML).toEqual(slug);
})

test('ReadOnlyComponent should render with default textid', () => {
	const slug = '/subject/biology/classification/general-biology/topic/0-biology'
    const { getByText } = renderComponent({ value: slug, testid: undefined});
    const result = getByText(slug);
    expect(result.getAttribute('data-testid')).toEqual('--read-only');
})