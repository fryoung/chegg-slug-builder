import React from 'react'
import Table from '@contentful/forma-36-react-components/dist/components/Table/Table'
import TableHead from '@contentful/forma-36-react-components/dist/components/Table/TableHead'
import TableBody from '@contentful/forma-36-react-components/dist/components/Table/TableBody'
import TableRow from '@contentful/forma-36-react-components/dist/components/Table/TableRow'
import TableCell from '@contentful/forma-36-react-components/dist/components/Table/TableCell'

export interface TaxonomyProps {
    fields: {
        subject?: string | null
        classification?: string | null
        topic?: string | null
    }
}

export default function Taxonomy({fields: {subject, classification, topic}}: TaxonomyProps): JSX.Element {
   return( <Table> 
        <TableHead>
        <TableRow>
            <TableCell>Subject</TableCell>
            <TableCell>Classification</TableCell>
            <TableCell>Topic</TableCell>
        </TableRow>
        </TableHead>
        <TableBody>
        <TableRow>
            <TableCell>{subject}</TableCell>
            <TableCell>{classification}</TableCell>
            <TableCell>{topic}</TableCell>
        </TableRow>
        </TableBody>
   </Table> )
}