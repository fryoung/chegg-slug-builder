interface KeyCodeProps {
    [key: string]: number
}

interface FieldProps {
    [key: string]: string
}

export const Fields: FieldProps = {
	SUBJECT: 'subject',
	CLASSIFICATION: 'classification',
	TOPIC: 'topic'
}

export const Keys: KeyCodeProps = {
	ENTER: 13	
}