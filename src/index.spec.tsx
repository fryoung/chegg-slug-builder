import React from 'react';
import { EditorExtensionSDK } from 'contentful-ui-extensions-sdk';
import { App } from './index';
import { render, cleanup, fireEvent, wait } from '@testing-library/react';


interface MockEntryField {
  getValue: jest.Mock,
  setValue: jest.Mock,
  onValueChanged?: jest.Mock
}

interface FakeSDK {
    notifier?: {
      success: (e:Event) => void
    },
    entry: {
      fields: { [key: string]: MockEntryField }
    }
}

function getSelectComponent(wrapper: HTMLDivElement): HTMLSelectElement {
  return wrapper.querySelector('select') as HTMLSelectElement;
}

function renderComponent(sdk: FakeSDK) {
  return render(<App sdk={sdk as unknown as EditorExtensionSDK} />);
}

const sdk:FakeSDK = {
  notifier: {
    success: jest.fn()
  },
  entry: {
    fields: {
      slug: { getValue: jest.fn() as jest.Mock, setValue: jest.fn() as jest.Mock, onValueChanged: jest.fn() },      
      title: { getValue: jest.fn(), setValue: jest.fn() as jest.Mock },
      subject: { getValue: jest.fn(), setValue: jest.fn() },
      classification: { getValue: jest.fn(), setValue: jest.fn() },
      topic: { getValue: jest.fn(), setValue: jest.fn() }
    }
  }
};


describe('App', () => {

  beforeEach(() => {
    jest.resetAllMocks();
  });

  afterEach(cleanup);

  it('should read a values from entry.fields.*', () => {
    const slug = '/subject/chemistry/classification/organic-chemistry/topic/0-organic-chemistry'
    const getVal: jest.Mock = sdk.entry.fields.slug.getValue as jest.Mock
    // set slug in the mock return value
    getVal.mockReturnValue(slug);

    const { getByTestId } = renderComponent(sdk);
    const slugFieldWrapper = getByTestId('field-slug') as HTMLDivElement;
    const slugInput = slugFieldWrapper.querySelector('#slug') as HTMLInputElement
    expect(slugInput.value).toEqual(slug);
  });

  it('should populate dropdowns if values exist already', () => {

    let mockSDK: FakeSDK = {
      ...sdk,
      entry: {
        fields: {
          slug: sdk.entry.fields.slug,
          subject: {           
            getValue: jest.fn(),
            setValue: jest.fn(),
          },
          classification: {
            getValue: jest.fn(),
            setValue: jest.fn(),
          },
          topic: {
            getValue: jest.fn(),
            setValue: jest.fn()
          }
        }
      }
    }

    mockSDK.entry.fields.subject.getValue.mockReturnValue('chemistry')
    mockSDK.entry.fields.classification.getValue.mockReturnValue('general chemistry')
    mockSDK.entry.fields.topic.getValue.mockReturnValue('0-general chemistry')

    const { getByText, getByTestId } = renderComponent(mockSDK);
    wait(()=>getByTestId('field-slug'));

    setTimeout(()=>{
      const fieldWrapper:any = getByTestId('field-slug') as HTMLDivElement
      console.log('fieldWrapper.querySelector("input").value', fieldWrapper.querySelector('input').value)
      //expect(fieldWrapper.querySelector('input').value).toEqual('/subject/chemistry/classification/general-chemistry/topic/0-general chemistry')
    }, 25)



  });

  it('should call setValue on title field when changed', async () => {
    const { getByTestId } = renderComponent(sdk);
    const titleField = getByTestId('field-title') as HTMLInputElement;
    fireEvent.change(titleField, {
      target: {
        value: 'hello world'
      }
    });
    expect(sdk.entry.fields.title.setValue).toHaveBeenCalledWith('hello world')
  });

  it('should render subject dropdown when slug is invalid or incomplete', async () => {
    const slug = ''
    sdk.entry.fields.slug.getValue.mockReturnValue(slug);
    const { getByRole, findByRole } = renderComponent(sdk);
    const editSlugButton = getByRole('button');
    fireEvent.click(editSlugButton);

    const selectField = await findByRole('listbox') as HTMLSelectElement;
    expect(selectField.value).not.toBeNull()
  });


  it('should open / close slug-builder when edit button and close buttons are clicked', async () => {
    const { queryByText, findByText, queryByRole, queryAllByRole, getByText} = renderComponent(sdk);

    const editSlugButton:HTMLButtonElement = await queryByText('edit slug') as HTMLButtonElement;
    fireEvent.click(editSlugButton);

    // wait for a sec until the slug builder appears
    wait(()=>getByText('Slug Builder'));
    
    const slugBuilder = await findByText('Slug Builder') as HTMLDivElement
    let parentElement = slugBuilder.parentElement
    if(parentElement) {
      parentElement = parentElement.parentElement as HTMLDivElement
    }

    const closeButton:any = parentElement ? parentElement.querySelector('.close-button') : undefined;
    fireEvent.click(closeButton);

    // slug builder should now be hiddem
    expect(queryByText('Slug Builder')).toBeNull();
  });


  it('should render subject/classification/topic dropdowns after selections are made', async () => {

    const { getByRole, findByTestId } = renderComponent(sdk);
    const editSlugButton = getByRole('button');
    fireEvent.click(editSlugButton);

    const subjectField = getSelectComponent(await findByTestId('cf-ui-select--subject') as HTMLDivElement);
    fireEvent.change(subjectField, {
      target: {
        value: 'engineering'
      }
    });

    const classificationsField = getSelectComponent(await findByTestId('cf-ui-select--classification') as HTMLDivElement);
    expect(classificationsField.options[1].value).toEqual('computer science');

    fireEvent.change(classificationsField, {
      target: {
        value: 'computer science'
      }
    });

    const topicsField = getSelectComponent(await findByTestId('cf-ui-select--topic') as HTMLDivElement);
    expect(topicsField.options[1].value).toEqual('0-computer science');

    fireEvent.change(topicsField, {
      target: {
        value: '0-computer science'
      }
    });

    type AsyncMock<T> = ()=>Promise<T>
    const slug = '/subject/engineering/classification/computer-science/topic/0-computer-science'
    const slugField:MockEntryField = sdk.entry.fields.slug as MockEntryField
    const fn:AsyncMock<string> = slugField ? await slugField.setValue(slug) : jest.fn()
    expect(slugField.setValue).toHaveBeenCalledWith(slug);

  });
})
