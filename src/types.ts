export interface GenericObject {
  [key: string]: string | null | undefined
}

export interface NameValuePair {
  name: string,
  value: string | null | undefined
}

export interface Query {
  [key: string]: string | null;
}

export interface SelectorField {
  id: string;
  name: string;
  value?: string | null;
  options?: OptionItem[];
}

export interface OptionItem {
  id: string;
  name: string;
  value: string;
}

export interface CheggData {
  [key: string]: SelectorField | string | null;
  subject: SelectorField | null;
  classification: SelectorField | null;
  topic: SelectorField | null;
}

export interface Subject {
  id: string
  classifications: Classification[]
}

export interface Classification {
  id: string
  name: string
  topics?: Topic[]
}

export interface Topic {
  id: string
  name: string
}



