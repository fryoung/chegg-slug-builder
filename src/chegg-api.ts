// import fetch from 'isomorphic-fetch';
import * as mocks from './mocks'
import { Fields} from './constants'
import {SelectorField, OptionItem, Query} from './types'

/**
 * Returns array of subjects
 * @return Array<any>
 */
export const getSubjects = async (opts:Query = {}):Promise<SelectorField> => {
  /* custom subject request logic goes here */
  // const response = await fetch('').then( res => res.json())

  /* mock response */ 
  const options = await mocks.getSubjects(opts) as OptionItem[];  
  return {
    id: Fields.SUBJECT,
    name: 'Subject',
    options,
  }
};


/**
 * Returns array of classifications
 * @param  {}
 * @return Array<any>
 */
export const getClassifications = async ({subject}:Query):Promise<SelectorField> => {
  /* custom classifications request logic goes here */
  // const response = await fetch('').then( res => res.json())

  /* mock response */
  const options = await mocks.getClassifications({subject}) as OptionItem[];
  return {
    id: Fields.CLASSIFICATION,
    name: 'Classification',
    options
  }  
};

/**
 * Returns array of topics
 * @param  {}
 * @return Array<any>
 */
export const getTopics = async ({subject, classification}:Query):Promise<SelectorField> => {
  /* custom topics request logic goes here */
  // const response = await fetch('').then( res => res.json())

  /* mock response */
  const options = await mocks.getTopics({subject, classification}) as OptionItem[];
  return {
    id: Fields.TOPIC,
    name: 'Topic',
    options
  }
};

/**
 * 
 * @param fieldName Given a fieldName, returns the corresponding async 
 */
export function getQuery(fieldName: string, query:Query): Promise<SelectorField> | null {
   // console.info(`getQuery("${fieldName}",${JSON.stringify(query)})`);
   switch(fieldName) {
     case Fields.SUBJECT:
         return getSubjects(query);
     case Fields.CLASSIFICATION:
        return getClassifications(query);
     case Fields.TOPIC:
        return getTopics(query);
   }
   return null;
}


/**
 * Return data field names
 * NOTE: This is for the mock JSON response
 * @see /src/data/mock-chegg-data.json
 */
export function getFieldNames():string[]{
  return [Fields.SUBJECT, Fields.CLASSIFICATION, Fields.TOPIC]
}

