import {Query, GenericObject} from '../types'
import { EditorExtensionSDK, EntryFieldAPI } from 'contentful-ui-extensions-sdk';

/**
 * Looks for duplicate entries for field value
 * @param field
 * @param sdk 
 */
export async function getDuplicates (field:GenericObject, sdk: EditorExtensionSDK) {
    
    if (!field) {
      return Promise.resolve(false)
    }

    // eslint-disable-next-line
    const sys:any = sdk.entry.getSys();

    const query:Query = {
      content_type: sys.contentType.sys.id,
      ['fields.' + field.id]: field.value,
      ['sys.id[ne]']: sys.id
    }

    return sdk.space.getEntries(query).then(function (result) {
      return result.total > 0
    })
}