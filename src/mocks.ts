import {Query, Subject, Classification} from './types'
import cheggData from './data/mock-chegg-data.json';

/**
 * mock response methods here
 */

const cache:{[key: string]: unknown} = {}

export async function getSubjects(query: Query = {} /* eslint-disable-line */ ) {
    // faking some latency
    return new Promise(resolve => setTimeout(() => {      
      const response = cheggData.subjects.map( ({id, name}:{id: string, name: string}) => ({
        id,
        name
      }));
      cache['subject'] = response; 
      resolve(response);
    }, Math.random() * 500));	
}

export async function getClassifications({subject}: Query) { 
    // faking some latency
    return new Promise(resolve => setTimeout(() => {
      const match = cheggData.subjects.find( (sbj:Subject ) => sbj.id === subject) 
      const response = match ? match.classifications.map( ({id, name}: Classification) => ({
        id,
        name
       })) : [];
       resolve(response);
  }, Math.random() * 500));
}

export async function getTopics({subject, classification}: Query) {
  // faking some latency
  return new Promise((resolve, reject) => setTimeout(() => {
      const match:Subject = cheggData.subjects.find( (sbj: Subject) => sbj.id === subject) as Subject;
      const clsf:Classification | undefined = match ? match.classifications.find( (cl: Classification) => cl.id === classification) as Classification : undefined;

      if(clsf) {
        resolve(clsf.topics);
      }
      else {
        reject(`could not find match with subject=${subject}, classification=${classification}`)
      }

  }, Math.random() * 1000));
}