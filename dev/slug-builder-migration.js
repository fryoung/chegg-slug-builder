module.exports = function (migration, context) {
  
  // { makeRequest, spaceId, accessToken }
  const widgetId = 'chegg-slug-builder';
  const cTypeName = 'SlugBuilderTest';
  const cTypeDesc = 'field for POC of Chegg slug-builder UI extension';  
  const cTypeId = cTypeName.toLowerCase();
  const fields = ['Title', 'Slug'];

  const cType = migration.createContentType(cTypeId, {
  	name: cTypeName,
    description: cTypeDesc
  });


  fields.forEach( fieldName => {
      let field = cType
        .createField(fieldName.toLowerCase())
        .name(fieldName)
        .type('Symbol')
        .required(true);
  });

 cType.configureEntryEditor ('extension', widgetId, {});

};