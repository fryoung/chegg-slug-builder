#!/usr/bin/env node 

/**
 * dev helper script
 * script to generate mock topics data for Chegg-Topic-Selector UI Extension for Contentful
 */

const path = require('path');
const fs = require('fs');


const outDir = __dirname;

function range(min, max) {
    return Math.random() * (max - min) + min
}


/**
* create some lorum ipsum topics
 */
function getTopics(subject, classification) {
    const count = range(4, 10);
    const topics = [];
    for( let i = 0; i <= count; i++) {
    	topics.push({
        id: `${i}-${classification.toLowerCase()}`,
    		name: `Sample ${subject}/${classification } topic ${i}`
    	});
    }

    return topics;
}

fs.readFile(path.resolve(__dirname,'./data.txt'), 'utf8', (err, data) => {
    if(err) {
    	console.error('Error reading subjects', err)
    }
    else {
    	const rows = data.split('\n');
    	const json = rows.reduce( (r, row) => {
    	  if(row && row.length) {
	        const pairs = row.split(':');
	        const subject = pairs[0];	    	   
	        const classifications = pairs[1].split(' | ');
	    	r.subjects.push({
            id: subject.toLowerCase(),
	    	    name: subject,
	    	    classifications: classifications.map( classification => ({
               id: classification.toLowerCase(),
	             name: classification,
	             topics: getTopics(subject, classification)
	    	    }))
	    	});
	        return r;
    	  }
    	}, {"subjects": []});

        const outPath = `${outDir}/mock-chegg-data.json`
    	fs.writeFile(outPath, JSON.stringify(json), err =>{
          if(err) {
          	console.error(err);
          }
          else {
              console.info(`data written to ${outPath}`);
          }
    	});
    }
});

