const { init } = require('./client');

/**
 * to run this command, enter contentType you want to search for
 * node ./get_entries.js
 */

async function listEntriesByContentType(environment, contentType) {
	console.info('listEntriesByContentType: ', contentType);
    const entryList = await environment.getEntries({'content_type': contentType});
    const entryListMapped = entryList.items.map( item => ({
    	id: item.sys.id,
    	type: item.sys.type,
    	environment: item.sys.environment.sys.id,
    	fields: item.fields
    }))
    console.info(JSON.stringify(entryListMapped, null, 4));
}

async function start() {
	const contentType = 'slugbuildertest'	
	const {client, space, env} = await init();
	if(env) {
	   listEntriesByContentType(env, contentType);
	}
	else {
		console.log('environment not loaded');
	}
}

start();
