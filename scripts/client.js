const {createClient} = require('contentful-management');

async function init() {

    const { managementToken, activeSpaceId, activeEnvironmentId} = require('../.contentfulrc.json')
	const client = await createClient({
		accessToken: managementToken
	});

	const space = await client.getSpace(activeSpaceId);
	const env = await space.getEnvironment(activeEnvironmentId);

	return {
		client,
		space,
		env
	}
}

module.exports = {
	init
};
